<?php
/*
Version: 1.0
Date: 2/21/2015 10:47 PM
Author: Rad
Author URI: http://radgh.com/

@package radgh
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <?php wp_head(); ?>
</head>

<body <?php body_class('front-page'); ?>>

<div id="front-page">
  <div class="container">
    <div class="inner">
      <article id="fp-main">

        <h1><a href="<?php echo esc_attr( get_bloginfo('url') ); ?>">Radley Sustaire</a></h1>
        <h2>Full Stack Web Developer &ndash; Eugene, OR</h2>

        <div class="skill-list">
          <div><strong>Web:</strong> WordPress, PHP, MySQL, JavaScript, jQuery, HTML, CSS</div>
          <div><strong>Server:</strong> Linux, Apache, Nginx, SSH, Bash, PCI Compliance</div>
          <div><strong>Software:</strong> PhpStorm, HeidiSQL, Photoshop, Illustrator, Git</div>
          <div><strong>Specialty:</strong> Responsive Design, Compatibility, User Experience</div>
        </div>

        <div class="button-row">
          <div class="button-col col-a">
            <a href="#" onclick="return false;" class="button hover-disabled">
              <span class="button-text">Portfolio</span>
              <span class="button-disable-text">Coming Soon</span>
            </a>
          </div>
          <div class="button-col col-b">
            <a href="https://docs.google.com/document/d/1_jFspDYesJVkLJ3wX3CtWxZ18gj90mx0dsDuQJHbmWQ/edit?usp=sharing" class="button" target="_blank" id="fp-btn-resume">
              <span class="button-text">Resume</span>
            </a>
          </div>
          <div class="button-col col-c">
            <a href="mailto:radleygh@gmail.com?body=Hello+Radley+Sustaire..." target="_blank" class="button" id="fp-btn-contact">
              <span class="button-text">Contact</span>
            </a>
          </div>

        </div>

      </article>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
</body>
</html>

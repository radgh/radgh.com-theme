jQuery(function($) {
  $("#fp-btn-contact").click( fp_click_contact );
  $("#fp-btn-resume").click( fp_click_resume );
});

function fp_click_resume(e) {
  var msg = "My resume is currently available as a Google Doc. " +
    "A web-based version with some additional details and images " +
    "will be coming soon.\n\nPress OK to visit Google Docs.";

  if ( !confirm( msg ) ) {
    e.preventDefault();
    return false;
  }
}

function fp_click_contact(e) {
  var msg = "A contact page is coming soon, for now contact me via:\n\n" +
    "Phone: 541.513.8145\n" +
    "Email: radleygh@gmail.com\n\n" +
    "Press OK to send me an email with your email client.";

  if ( !confirm( msg ) ) {
    e.preventDefault();
    return false;
  }
}